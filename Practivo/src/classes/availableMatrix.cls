public class availableMatrix
{
    public boolean isTimeSlotPresent; //Service Provider is not present on this day
    public boolean isAllDayFull; //check for available for entire day.
    public Integer dayCode;
    public map<Integer,Integer> timeSlotMatrix;
    public availableMatrix()
    {
        isTimeSlotPresent = true;
        isAllDayFull=false;
        timeSlotMatrix = new map<Integer,Integer>();
    }
    public void checkForAllSlotFull()
    {
        integer setSize=0,index=0;
        setSize = this.timeSlotMatrix.KeySet().size();
        for(Integer itr : this.timeSlotMatrix.KeySet())
        {
            integer val = this.timeSlotMatrix.get(itr);
            if(val>=1)  
                index++;
        }
        if(setSize==index)
            this.isAllDayFull = true;
    }
}