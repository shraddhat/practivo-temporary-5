@RestResource(urlMapping='/endUserWidget') 
global class endUserWidgetAPI{
    
    @HttpGet
    global static void getLeftPane()
    {
        Utils.retriveLocationHelper returnObject= new Utils.retriveLocationHelper();
        RestContext.response.addHeader('Content-Type', 'application/json');
        RestContext.response.addHeader('Access-Control-Allow-Origin', '*');
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(returnObject));
 
    }
    @HttpPost
    global static void getAvailableCalendar()    
    {
        string str='this is fake response';
        system.debug('--- RetriveLocations :getCalendarForEndUser' + RestContext.request.requestBody.toString());
        RestContext.response.addHeader('Content-Type', 'application/json');
        RestContext.response.addHeader('Access-Control-Allow-Origin', '*');
        RestContext.response.responseBody = Blob.valueOf(RestContext.request.requestBody.toString());
            
    }
    
    public class responseClass
    {
    	string key;
    	string values;
    	public responseClass()
    	{}
    }   
}