/*
***************************************
 class name-TestDataUtils
 Class Description-Class for creating test data for test classes.
 *************************************
*/
@istest
public class TestDataUtils 
{
    public static contact getcontact()
    {
        Contact con =  new Contact();
        con.FirstName = 'Anil';
        con.LastName = 'Dutt';
        con.Email = 'anil@swiftsestup.com';
        con.Birthdate=date.valueOf('1994-09-12');
        insert con;
        return con;
    }
    
    public static eventrelation geteventrelation(event Eventdetails,contact con)
    {
        EventRelation evrel=new EventRelation();
        evrel.EventId=Eventdetails.id;
        evrel.RelationId=con.id;
        evrel.isparent=true;
        insert evrel;
        return evrel;
    }
    public static location__c getlocation()
    {
        Location__c loc=new Location__c();
        loc.name='TestLocation';
        loc.Address__c='Test adsress';
        insert loc;
        return loc;
    }
    public static category__c getcategory()
    {
       Category__c cat=new Category__c();
        cat.Name='physio';
        insert cat;
        return cat;
    }
    public static AppointmentType__c getappointment(category__c cat)
    {
        AppointmentType__c apt= new AppointmentType__c();
        apt.Name='app1';
        apt.Category__c=cat.id;
        apt.Maximum_Participant__c=1;
        apt.DefaultDuration__c=60;
        apt.Price__c=100;
        insert apt;
        return apt;
    }
    public static Location_Category_Mapping__c getlocationmapping(location__c loc,category__c cat,contact con)
    {
        Location_Category_Mapping__c locmap=new Location_Category_Mapping__c();
        locmap.Location__c=loc.id;
        locmap.Category__c=cat.id;
        locmap.Contact__c=con.id;
        insert locmap;
        return locmap;
    }
    public static Event geteventappointment(contact con)
    {
        Date dToday = System.Today(); 
          Event Eventdetails=new event();
        Eventdetails.subject='sample';
        Eventdetails.Event_Type__c='Appointment';
        Eventdetails.whoid=con.Id;
        Eventdetails.IsRecurrence=false;
        Eventdetails.StartDateTime=datetime.newInstance(dToday.year(), dToday.month(),dToday.day(),01,00,00); 
        Eventdetails.EndDateTime=datetime.newInstance(dToday.year(), dToday.month(),dToday.day(),02,00,00); 
        insert Eventdetails;
        return Eventdetails;
    }
    public static Event geteventappointmentwithall(contact con,Location_Category_Mapping__c locmap,AppointmentType__c apt)
    {
        Date dToday = System.Today();
          Event Eventdetails=new event();
        Eventdetails.subject='sample';
        Eventdetails.Event_Type__c='Appointment';
        Eventdetails.whoid=con.Id;
        Eventdetails.IsRecurrence=false;
        Eventdetails.Location_Category_Mapping__c=locmap.Id;
        Eventdetails.Appointment_Type__c=apt.id;
        Eventdetails.StartDateTime=datetime.newInstance(dToday.year(), dToday.month(),dToday.day(),01,00,00); 
        Eventdetails.EndDateTime=datetime.newInstance(dToday.year(), dToday.month(),dToday.day(),02,00,00);
        Eventdetails.No_of_Participants__c=1;
        insert Eventdetails;
        return Eventdetails;
    }
     public static Event getAppointmentWithBookedResource(contact con,Location_Category_Mapping__c locmap,AppointmentType__c apt,Booked_Resources__c bookedRes)
    {
        Date dToday = System.Today();
        Event Eventdetails=new event();
        Eventdetails.subject='sample';
        Eventdetails.Event_Type__c='Appointment';
        Eventdetails.whoid=con.Id;
        Eventdetails.IsRecurrence=false;
        Eventdetails.Location_Category_Mapping__c=locmap.Id;
        Eventdetails.Appointment_Type__c=apt.id;
        Eventdetails.StartDateTime=datetime.newInstance(dToday.year(), dToday.month(),dToday.day(),15,00,00); 
        Eventdetails.EndDateTime=datetime.newInstance(dToday.year(), dToday.month(),dToday.day(),16,00,00);
        Eventdetails.WhatId = bookedRes.Id;
        Eventdetails.No_of_Participants__c=0;
        insert Eventdetails;
        return Eventdetails;
    }
    public static Booked_Resources__c getbookedresources()
    {
        Booked_Resources__c br=new Booked_Resources__c();
        insert br;
        return br;
    }
    Public static Resource_Type__c getResourceType()
    {
        Resource_Type__c rt= new Resource_Type__c();
        rt.Type_of_Resource__c='Human Resource(Service Provider)';
      
        rt.Description__c='ada';
        insert rt;
        return rt;
    }
    Public static Resource_Type__c getResourceTypeAssistant()
    {
        Resource_Type__c rt= new Resource_Type__c();
        rt.Type_of_Resource__c='Human Resource(Non Service Provider)';
      
        rt.Description__c='ada';
        insert rt;
        return rt;
    }
        Public static Resource_Type__c getResourceTypeNH()
    {
        Resource_Type__c rt= new Resource_Type__c();
        rt.Type_of_Resource__c='Non Human Resource';
       
        rt.Description__c='ada';
        insert rt;
        return rt;
    }

    public static Resources__c getresources(location__c loc,Resource_Type__c rt)
    {
        Resources__c res=new Resources__c();
        res.name='Name'+system.now();
        res.Location__c=loc.id;
        res.Resource_Type__c=rt.id;
        res.Over_Booking_Limit__c =1;
        insert res;
        return res;
    }
    public static Resource_Type_Appointment_Type_Mapping__c getresaptypemapping(Resource_Type__c rt,AppointmentType__c apptyp)
    {
        Resource_Type_Appointment_Type_Mapping__c rtatmap=new Resource_Type_Appointment_Type_Mapping__c();
        rtatmap.Resource_Type__c=rt.id;
        rtatmap.Appointment_Type__c=apptyp.id;
        rtatmap.No_of_Resources_Required__c=1;
        insert rtatmap;
        return rtatmap;
    }
    public static Resource_Type_Appointment_Type_Mapping__c getresaptypemappingSpecific(Resource_Type__c rt,AppointmentType__c apptyp,Contact Sp)
    {
        Resource_Type_Appointment_Type_Mapping__c rtatmap=new Resource_Type_Appointment_Type_Mapping__c();
        rtatmap.Resource_Type__c=rt.id;
        rtatmap.Appointment_Type__c=apptyp.id;
        rtatmap.No_of_Resources_Required__c=1;
        rtatmap.isSpecific__c=true;
        rtatmap.Specific_Resource_Id__c=String.valueOf(Sp.id);
        insert rtatmap;
        return rtatmap;
    }
    public static Actual_Resource_Booked_Mapping__c getactualbooked(Resources__c res,Booked_Resources__c book)
    {
        Actual_Resource_Booked_Mapping__c arbookmap=new Actual_Resource_Booked_Mapping__c();
        arbookmap.Booked_Resources__c=book.id;
        arbookmap.Resources__c=res.id;
        arbookmap.No_Of_Instance_Used__c=1;
        insert arbookmap;
        return arbookmap;
    }
    public static Actual_Resource_Booked_Mapping__c getactualbookedAssistant(Contact ass,Booked_Resources__c book)
    {
        Actual_Resource_Booked_Mapping__c arbookmap=new Actual_Resource_Booked_Mapping__c();
        arbookmap.Booked_Resources__c=book.id;
        arbookmap.Assistants__c=ass.id;
        arbookmap.No_Of_Instance_Used__c=1;
        arbookmap.doNotOverbook__c=false;
        insert arbookmap;
        return arbookmap;
    }
    public static contact getServiceProvider()
    {
        RecordType recordTyp=[select id from Recordtype where Recordtype.developername='Service_Provider' and SobjectType='Contact'];
        Id RecTypeId = recordTyp.Id;
        Contact serviceProvider = new Contact();
        serviceProvider.FirstName = 'Test_SP';
        serviceProvider.LastName = 'ServiceProvider';
        serviceProvider.Email = 'test@test.com';
        serviceProvider.Birthdate=date.valueOf('1991-09-12');
        serviceProvider.RecordTypeId = RecTypeId; 
        insert serviceProvider;
        return serviceProvider;
    }
     public static contact getServiceProviderWithResTyp(Resource_Type__c resTyp)
    {
        RecordType recordTyp=[select id from Recordtype where Recordtype.developername='Service_Provider' and SobjectType='Contact'];
        Id RecTypeId = recordTyp.Id;
        Contact serviceProvider = new Contact();
        serviceProvider.FirstName = 'Test_SP';
        serviceProvider.LastName = 'ServiceProvider';
        serviceProvider.Email = 'test@test.com';
        serviceProvider.Birthdate=date.valueOf('1991-09-12');
        serviceProvider.RecordTypeId = RecTypeId; 
        serviceProvider.Resource_Type__c = resTyp.Id;
        insert serviceProvider;
        return serviceProvider;
    }
     public static contact getCustomer()
    {
        RecordType recordTyp=[select id from Recordtype where Recordtype.developername='Customer' and SobjectType='Contact'];
        Id RecTypeId = recordTyp.Id;
        Contact customer = new Contact();
        customer.FirstName = 'Test_Customer';
        customer.LastName = 'Customer';
        customer.Email = 'test@test.com';
        customer.Birthdate=date.valueOf('1991-09-12');
        customer.RecordTypeId = RecTypeId; 
        insert customer;
        return customer;
    }
    public static contact getAssistant(Resource_Type__c rs)
    {
        RecordType recordTyp=[select id from Recordtype where Recordtype.developername='Assistant' and SobjectType='Contact'];
        Id RecTypeId = recordTyp.Id;
        Contact Assistant = new Contact();
        Assistant.FirstName = 'Test_AS';
        Assistant.LastName = 'Assistant';
        Assistant.Email = 'test@testas.com';
        Assistant.Birthdate=date.valueOf('1991-09-12');
        Assistant.RecordTypeId = RecTypeId; 
        Assistant.Resource_Type__c=rs.id;
        insert Assistant;
        return Assistant;
    }
    public static Event geteventworkingtimeslot(contact con)
    {
        Date StartDate1 = Date.Today();
        StartDate1=StartDate1.addDays(-7);
        Time tym = Time.newInstance(9, 0, 0, 0);
        DateTime StartDateTime1 =  DateTime.newInstance(StartDate1, tym);
        system.debug('StartDateTime1'+StartDateTime1);
        Date EndDate = StartDate1.addDays(99);
        Event newEvent = new Event();
        newEvent.Subject ='Free Slot Assistant Adolf Marries';
        newEvent.WhoId = con.id;
        newEvent.Event_Type__c = 'Working_TimeSlot'; 
        newEvent.IsRecurrence = true;
        newEvent.RecurrenceStartDateTime= StartDateTime1;
        newEvent.RecurrenceEndDateOnly = EndDate;
        newEvent.RecurrenceType = 'RecursDaily';
        newEvent.RecurrenceInterval = 1; // This means that the event will wait 1 day before recurring again
        newEvent.DurationInMinutes =300;
        insert newEvent;
        return newEvent;
    }
    public static event insertWorkingTimeSlot(contact con,Resource_Type__c ResTyp,Location_Category_Mapping__c lcm)
    {
        Date StartDate1 = Date.Today();
        StartDate1=StartDate1.toStartOfWeek();
        Time tym = Time.newInstance(9, 0, 0, 0);
        DateTime StartDateTime1 =  DateTime.newInstance(StartDate1, tym);
        system.debug('StartDateTime1'+StartDateTime1);
        Date EndDate = StartDate1.addDays(99);
        Event newEvent = new Event();
        newEvent.WhatId = ResTyp.Id;
        newEvent.Subject ='Free Slot Assistant Adolf Marries';
        newEvent.WhoId = con.id;
        newEvent.Event_Type__c = 'Working_TimeSlot'; 
        newEvent.IsRecurrence = true;
        newEvent.RecurrenceStartDateTime= StartDateTime1;
        newEvent.RecurrenceEndDateOnly = EndDate;
        newEvent.RecurrenceType = 'RecursDaily';
        newEvent.RecurrenceInterval = 1; // This means that the event will wait 1 day before recurring again
        newEvent.DurationInMinutes =720;
        newEvent.Location_Category_Mapping__c = lcm.id;
        insert newEvent;
        return newEvent;
    }
}