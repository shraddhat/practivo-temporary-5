@isTest
public class Test_utils 
{
 static testmethod void testutil()
 {
     map<String,String> maplocale=new map<string,string>();
     test.startTest();
     Utils util=new Utils();
     string sampledate='10/12/2015';
     Utils.parseDate(sampledate);
     Utils.isEndTimePresent();
     Utils.isStartTimePresent();
     Utils.insertEndCustomSettings();
     Utils.insertStartCustomSettings();
     maplocale=utils.LOCALE_MAP;
     test.stopTest();
     System.assertNotEquals(null, maplocale);
 }
}