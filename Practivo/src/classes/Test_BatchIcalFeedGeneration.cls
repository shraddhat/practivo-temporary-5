@isTest
public class Test_BatchIcalFeedGeneration 
{
    public static String CRON_EXP = '0 30 * * * ?';
    static testmethod void testdata()
    {
        String query = 'SELECT Id, Name FROM Contact WHERE RecordType.developername = \'Service_Provider\' LIMIT 200';
         Contact[] ml = new List<Contact>();
           for (Integer i=0;i<10;i++) {
               Contact m = new Contact(
                   LastName='safafafa');
               ml.add(m);
           }
           insert ml;
        Test.startTest();
          BatchIcalFeedGeneration bi= new BatchIcalFeedGeneration();
        Database.executeBatch(bi);
           Test.stopTest();
        EpracticeSettings__c schedulerSettings= EpracticeSettings__c.getInstance('ScheduleJobLastRunDateTime');
        System.assertNotEquals(null,schedulerSettings);
        System.assertEquals('ScheduleJobLastRunDateTime',schedulerSettings.Name);
    }
        static testmethod void testjob()
    {
        
          Test.startTest();
    
          // Schedule the test job
          String jobId = System.schedule('ScheduleApexClassTest',
                            CRON_EXP, 
                            new BatchIcalFeedGeneration());
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
         NextFireTime
         FROM CronTrigger WHERE id = :jobId];
        System.assertNotEquals(null, ct);
        System.assertEquals(0, ct.TimesTriggered);
           test.stopTest();
    }
}