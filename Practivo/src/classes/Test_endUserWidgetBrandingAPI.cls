@isTest
public class Test_endUserWidgetBrandingAPI 
{
    Static testmethod void testBrandingAPIendWidget()
    {
        list<Branding__c> brandingList=new list<Branding__c>();
            brandingList.add(new Branding__c(name='Book_Appointment_Button_Color',value__c='#FFAA75'));
            brandingList.add(new Branding__c(name='Book_Appointment_Button_Text',value__c=''));
            brandingList.add(new Branding__c(name='Book_Appointment_Button_Text_Color',value__c='#FFAA75'));
        insert brandingList;
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/endUserWidget/branding';
        req.httpMethod = 'POST';
        req.addParameter('brandingCallFor','getBrandingEndUserWidget');
        RestContext.request = req;
        RestContext.response = res;
        endUserWidgetBrandingAPI.getBrandingInformation();
        System.assertNotEquals(null, res.responseBody);
    }
    Static testmethod void testBrandingAPIendbookappbutton()
    {
        list<Branding__c> brandingList=new list<Branding__c>();
            brandingList.add(new Branding__c(name='Book_Appointment_Button_Color',value__c='#FFAA75'));
            brandingList.add(new Branding__c(name='Book_Appointment_Button_Text',value__c=''));
            brandingList.add(new Branding__c(name='Book_Appointment_Button_Text_Color',value__c='#FFAA75'));
        insert brandingList;
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/endUserWidget/branding';
        req.httpMethod = 'POST';
        req.addParameter('brandingCallFor','getBookAppointmentButtonInfo');
        RestContext.request = req;
        RestContext.response = res;
        endUserWidgetBrandingAPI.getBrandingInformation();
        System.assertNotEquals(null, res.responseBody);
    }
}